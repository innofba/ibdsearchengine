package me.fba.hadoop.search;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class WikiArticleMapper extends Mapper<LongWritable, Text, LongWritable, WikiArticle> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        WikiArticle wikiArticle = new ObjectMapper().readValue(value.toString(), WikiArticle.class);

        context.write(new LongWritable(wikiArticle.getId()), wikiArticle);
    }
}
