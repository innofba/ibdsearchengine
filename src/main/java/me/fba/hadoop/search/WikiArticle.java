package me.fba.hadoop.search;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class WikiArticle implements WritableComparable<WikiArticle> {

    private LongWritable id = new LongWritable();;
    private Text url = new Text();
    private Text title = new Text();
    private Text text = new Text();
    private DoubleWritable relevance = new DoubleWritable();

    public Double getRelevance() {
        return relevance.get();
    }

    public void setRelevance(Double relevance) {
        this.relevance = new DoubleWritable(relevance);
    }

    public WikiArticle() {
    }

    public long getId() {
        return id.get();
    }

    public void setId(long id) {
        this.id = new LongWritable(id);
    }

    public void setUrl(String url) {
        this.url = new Text(url);
    }

    public void setTitle(String title) {
        this.title = new Text(title);
    }

    public void setText(String text) {
        this.text = new Text(text);
    }

    public String getUrl() {
        return url.toString();
    }

    public String getTitle() {
        return title.toString();
    }

    public String getText() {
        return text.toString();
    }

    @Override
    public void write(DataOutput out) throws IOException {
        id.write(out);
        url.write(out);
        title.write(out);
        text.write(out);
        relevance.write(out);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        id.readFields(in);
        url.readFields(in);
        title.readFields(in);
        text.readFields(in);
        relevance.readFields(in);
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int compareTo(WikiArticle o) {
        return Long.compare(getId(), o.getId());
    }
}
