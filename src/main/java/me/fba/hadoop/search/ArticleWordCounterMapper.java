package me.fba.hadoop.search;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class ArticleWordCounterMapper extends Mapper<LongWritable, WikiArticle, WikiArticle, MapWritable> {
    @Override
    protected void map(LongWritable key, WikiArticle value, Context context) throws IOException, InterruptedException {
        Map<String, Integer> wordCount = Arrays.stream(value.getText().replaceAll("[^A-z]", " ").split("\\s"))
                .map(String::trim)
                .map(String::toLowerCase)
                .filter(s -> !s.isEmpty())
                .collect(Collectors.toMap(o -> o, o -> 1, Integer::sum));

        MapWritable mapWritable = new MapWritable();

        wordCount.forEach((k, v) -> {
            mapWritable.put(new Text(k), new IntWritable(v));
        });

        context.write(value, mapWritable);
    }
}
