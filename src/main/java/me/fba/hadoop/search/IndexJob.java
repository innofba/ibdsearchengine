package me.fba.hadoop.search;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.chain.ChainMapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.lang.IllegalArgumentException;

public class IndexJob {
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException, URISyntaxException {
        if (args.length != 1)
	   throw new IllegalArgumentException("Usage: <program> <path_to_folder>");

	Configuration conf = new Configuration();

        System.setProperty("HADOOP_USER_NAME", "cairo");
        conf.set("fs.defaultFS", "hdfs://10.90.138.32:9000");
        conf.set("mapred.tasktracker.map.tasks.maximum", "8");

        Job job = Job.getInstance(conf, "Wordcount");
        job.setJarByClass(IndexJob.class);

        Path outputPath = new Path("/user/cairo/searchindex");
        FileSystem fs = FileSystem.get(new URI(outputPath.toString()), conf);
        fs.delete(outputPath, true);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, outputPath);

        ChainMapper.addMapper(job, WikiArticleMapper.class,
                LongWritable.class, Text.class, LongWritable.class, WikiArticle.class, conf);
        ChainMapper.addMapper(job, ArticleWordCounterMapper.class,
                LongWritable.class, WikiArticle.class, WikiArticle.class, MapWritable.class, conf);

        job.setOutputKeyClass(WikiArticle.class);
        job.setOutputValueClass(MapWritable.class);

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
