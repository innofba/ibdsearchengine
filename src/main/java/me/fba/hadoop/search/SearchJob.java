package me.fba.hadoop.search;

import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.lang.IllegalArgumentException;

public class SearchJob {
    public static void main(String[] args) throws IOException, URISyntaxException, ClassNotFoundException, InterruptedException {
        if (args.length != 2)
	   throw new IllegalArgumentException("Usage: <program> <number_of_results> <query>");

	try {
           Integer.parseInt(args[0]);
	} catch (Exception e) {
	   throw new IllegalArgumentException("Usage: <program> <number_of_results> <query>");
	}
	Configuration conf = new Configuration();

        System.setProperty("HADOOP_USER_NAME", "cairo");
        conf.set("fs.defaultFS", "hdfs://10.90.138.32:9000");
        conf.set("mapred.tasktracker.map.tasks.maximum", "8");
        conf.set("_query", args[1]);

        Job job = Job.getInstance(conf, "Search");
        job.setJarByClass(SearchJob.class);

        Path outputPath = new Path("/user/cairo/searchresult");
        FileSystem fs = FileSystem.get(new URI(outputPath.toString()), conf);
        fs.delete(outputPath, true);

        FileInputFormat.addInputPath(job, new Path("/user/cairo/searchindex"));
        FileOutputFormat.setOutputPath(job, outputPath);

        job.setMapperClass(RelevanceMapper.class);
        job.setSortComparatorClass(LongWritable.DecreasingComparator.class);

        job.setOutputKeyClass(DoubleWritable.class);
        job.setOutputValueClass(Text.class);

        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

	job.waitForCompletion(true);

	conf.set("fs.default.name", "hdfs://10.90.138.32:9000");

        String content = getContent("/user/cairo/searchresult/part-r-00000", FileSystem.get(conf));
        String[] lines = content.split("\\n");

        for (int i = 0; i < Math.min(Integer.parseInt(args[0]), lines.length); i++) {
	    String[] res = lines[i].split("\\t");
	    if (res.length > 1) {
                String _res = res[1];
            	System.out.println(_res);
	    }
        }

        System.exit(0);
    }

    private static String getContent(String path, FileSystem fs) {
        try {
            FSDataInputStream inputStream = fs.open(new Path(path));
            String out = IOUtils.toString(inputStream, "UTF-8");
            inputStream.close();
            return out;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    private static class RelevanceMapper extends Mapper<WikiArticle, MapWritable, DoubleWritable, Text> {
        @Override
        protected void map(WikiArticle key, MapWritable value, Context context) throws IOException, InterruptedException {
            String query = context.getConfiguration().get("_query").toLowerCase();
            String[] queryWords = query.split("\\s");

            double relevance = 0;
            for (String queryWord : queryWords) {
                if (value.containsKey(new Text(queryWord)))
                    relevance += ((IntWritable) value.get(new Text(queryWord))).get();
            }

            key.setRelevance(relevance);

            if (relevance > 0)
                context.write(new DoubleWritable(key.getRelevance()), new Text("title: " + key.getTitle() + 
					" rank: " + key.getRelevance()));

        }
    }
}
